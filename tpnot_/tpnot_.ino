#include <TFT_eSPI.h> 
#include <LIS3DHTR.h>

TFT_eSPI tft = TFT_eSPI();
LIS3DHTR<TwoWire> lis; 
//Constantes :
//couleurs : 
#define TFT_BLACK       0x0000      /*   0,   0,   0 */
#define TFT_NAVY        0x000F      /*   0,   0, 128 */
#define TFT_DARKGREEN   0x03E0      /*   0, 128,   0 */
#define TFT_DARKCYAN    0x03EF      /*   0, 128, 128 */
#define TFT_MAROON      0x7800      /* 128,   0,   0 */
#define TFT_PURPLE      0x780F      /* 128,   0, 128 */
#define TFT_OLIVE       0x7BE0      /* 128, 128,   0 */
#define TFT_LIGHTGREY   0xC618      /* 192, 192, 192 */
#define TFT_DARKGREY    0x7BEF      /* 128, 128, 128 */
#define TFT_BLUE        0x001F      /*   0,   0, 255 */
#define TFT_GREEN       0x07E0      /*   0, 255,   0 */
#define TFT_CYAN        0x07FF      /*   0, 255, 255 */
#define TFT_RED         0xF800      /* 255,   0,   0 */
#define TFT_MAGENTA     0xF81F      /* 255,   0, 255 */
#define TFT_YELLOW      0xFFE0      /* 255, 255,   0 */
#define TFT_WHITE       0xFFFF      /* 255, 255, 255 */
#define TFT_ORANGE      0xFDA0      /* 255, 180,   0 */
#define TFT_GREENYELLOW 0xB7E0      /* 180, 255,   0 */

#define SPEEDGAME 5
// Paramètres du jeu : 
bool isGameRunning = false;
unsigned long lastTimeDecrement = 0; // Stocke le moment où la dernière seconde a été décrémentée
unsigned long timeInterval = 1000; // Interval de temps en millisecondes (1000 ms = 1 seconde)
int score = 0;            // Initialisation du score
int timeLeft = 30;        // Initialisation du timer
String oldLevel = "";     // Niveau précédent
int oldScore = -1;        // Score précédent
int oldTimeLeft = -1;     // Temps précédent
String level = "Novice";  // Initialisation du niveau (changeable après)
int ballRadius = 7; // rayon de la boule rouge
int ballX = 160; // coordonné x du centre de la boule
int ballY = 120; // coordonné y du centre de la boule
int oldBallX = ballX; // stocker les anciennes coordonnées de la balle
int oldBallY = ballY; // stocker les anciennes coordonnées de la balle

// variable pour stocker les données de l'accèléromètre
float accX, accY;

void setup() {
  Serial.begin(115200);
  pinMode(WIO_KEY_A, INPUT_PULLUP);
  pinMode(WIO_KEY_B, INPUT_PULLUP);
  pinMode(WIO_KEY_C, INPUT_PULLUP);

  

  pinMode(2, INPUT); // définir la pin du bouton du milieu comme entrée
  tft.begin();            // Démarage de l'écran
  tft.setRotation(3);     // Oriente l'écran dans le bon sens
  // dessine la bannière bleu
  tft.fillRoundRect(0, 0, 320, 40, 0, TFT_BLUE);
  tft.fillRoundRect(0, 40, 320, 200, 0, TFT_DARKGREY);
  //set la couleur du text
  tft.setTextColor(TFT_WHITE, TFT_BLUE); 
  tft.setTextSize(2);
  // Affichage du niveau, score et timer
  tft.drawString(level, 10, 10);
  tft.drawString(String(score)+" pts", 160, 10);
  tft.drawString(String(timeLeft), 290, 10);
  //dessine la boule rouge :
  tft.fillCircle(ballX, ballY, ballRadius, TFT_RED);
  //
  lis.begin(Wire1);
  if (!lis) {
    while (1);
  }
  lis.setOutputDataRate(LIS3DHTR_DATARATE_25HZ);
  lis.setFullScaleRange(LIS3DHTR_RANGE_2G);
}

void updateBanner() {
  // Vérifier si les valeurs ont changé
  if (level != oldLevel || score != oldScore || timeLeft != oldTimeLeft) {
    // Actualiser le bandeau
    tft.fillRoundRect(0, 0, 320, 40, 0, TFT_BLUE);
    tft.drawString(level, 10, 10);
    tft.drawString(String(score) + " pts", 160, 10);
    tft.drawString(String(timeLeft), 290, 10);
    // Mettre à jour les valeurs précédentes
    oldLevel = level;
    oldScore = score;
    oldTimeLeft = timeLeft;
  }
}

void loop() {
  int buttonState = digitalRead(WIO_KEY_B);

  if(buttonState == LOW){
    isGameRunning = true;
    lastTimeDecrement = millis();
  }

  if(buttonState == LOW){
    isGameRunning = true;
    lastTimeDecrement = millis();
  }
  //Si le jeu n'est pas en cours, ne fait rien d'autre
  if(!isGameRunning){
    return;
  }
  delay(10);
  // Lire les données de l'accéléromètre
  accX = -lis.getAccelerationY(); 
  accY = lis.getAccelerationX(); 

  // Nettoyer l'ancienne position de la balle
  tft.fillCircle(oldBallX, oldBallY, ballRadius, TFT_DARKGREY);

  // Déplacer la balle en fonction des données de l'accéléromètre
  ballX += accX * SPEEDGAME;
  ballY += accY * SPEEDGAME;

  // Limiter les positions de la balle à l'intérieur de la zone de jeu
  if (ballX < 0 + ballRadius) ballX = ballRadius;
  if (ballX > 320 - ballRadius) ballX = 320 - ballRadius;
  if (ballY < 40 + ballRadius) ballY = 40 + ballRadius;
  if (ballY > 240 - ballRadius) ballY = 240 - ballRadius;
  // Gestion du chronomètre
  if (millis() - lastTimeDecrement >= timeInterval) {
    // Une seconde s'est écoulée, décrémentez le timer et mettez à jour le moment du dernier décrément
    timeLeft--;
    lastTimeDecrement = millis();
  }
  // Si le temps est écoulé, arrêtez le jeu
  if (timeLeft <= 0) {
    isGameRunning = false;
  }
  // Mettre à jour le bandeau si nécessaire
  updateBanner();
  // Dessiner la nouvelle position de la balle
  tft.fillCircle(ballX, ballY, ballRadius, TFT_RED);

  // Mettre à jour les anciennes coordonnées de la balle
  oldBallX = ballX;
  oldBallY = ballY;
}
